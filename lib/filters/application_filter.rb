module Filters
  
  class ApplicationFilter
    def run_filters()
      if @filters       
        @filters.each do |filter|  
          apply_filter filter
        end
      end
      @resource
    end
    
    def except(id)
      @resource.where("id != ?", id)
    end
    
    private
    
    def apply_filter(filter)
      if filter[1] and not filter[1].empty?
        @resource = self.send(filter[0], filter[1])
      else
        @resource = self.send(filter[0])
      end
    end
  end
  
end