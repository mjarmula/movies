module Filters
  
  class UsersFilter < ApplicationFilter
    def initialize(controller)
      @resource = User.all
      @controller = controller
      @filters = controller.params[:filters]
    end
    
    def current_user
      @resource.get_user(@controller.current_user.id)
    end
    
    def friends
      @controller.current_user.my_friends
    end
    
    def without_friends
      my_friends_ids = @controller.current_user.my_friends.map {|user| user.id}
      @resource.where.not(id: my_friends_ids)
    end   
  end
  
end