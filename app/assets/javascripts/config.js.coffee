@app.config(($httpProvider, $locationProvider) ->
  $httpProvider.interceptors.push(($rootScope, $location, Alerts)->
    return {
      responseError: (response)->
        if(response.status == 401)
          Alerts.add_alert {type: "danger", msg: "Authentication failed"}
          console.warn response,'response'
          $rootScope.login = true
          return {}
        else
          return response
      ,
      response: (response)->
        if response.data.alert
          Alerts.add_alert response.data.alert
        response
        
    }
  )
)

.config( ($routeProvider) ->
  $routeProvider.when('/', {
    templateUrl: '/?disable_layout=true'
  })
  .when('/users', {
      templateUrl: '/users?disable_layout=true',
  })
  .when('/users/:id', {
      templateUrl: (params)->
        '/users/'+params.id+'?disable_layout=true'
  })
  .otherwise({redirectTo:'/'})
)