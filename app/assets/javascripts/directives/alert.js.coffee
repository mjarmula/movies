@app.directive 'alerts', ()->
  {
    restrict: 'E',
    template: '<div uib-alert ng-repeat="alert in alerts" ng-class="\'alert-\' + (alert.type || \'warning\')" close="close_alert(alert)" class="main-alert" ng-if="alert">{{alert.msg}}</div>',
    scope: {},
    controller: ($scope, $rootScope, Alerts)->
      $scope.alerts = Alerts.alerts
      
      $scope.close_alert = (alert)->
        Alerts.remove_alert alert
  }