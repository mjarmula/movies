@app.directive 'login', ()->
  {
    restrict: 'E'
    templateUrl: '/users/sign_in?disable_layout=true',
    controller: ($scope, $rootScope, Auth)->
      $scope.form_error = {}
      $scope.mode = 'login'
      $scope.login = ->
        Auth.login($scope.data).then((result)->
          $scope.form_error = result
        )
      $scope.register = ->
        $scope.mode = 'register'
        Auth.register($scope.data).then((result)=>
          $scope.form_error = result
        )
      $scope.goTo = (mode)->
        $scope.mode = mode      
  }