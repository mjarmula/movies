class Auth extends Resource
  constructor: ($rootScope)->
    super
    @set_path('/users.json')
    @set_filter('current_user')
    @rootScope = $rootScope
    @user = {}
    @set_user()
    
  current_user: ()->
    @user

  set_user: () ->
    @set_path('/users.json')
    @set_filter('current_user')
    @query().$promise.then((data) =>
      if data
        angular.extend @user, data[0] if data[0]
        console.warn @user, 'set_user'
    )
    
  login: (data)->
    @set_path('/users/sign_in.json')
    @save(data).$promise.then((data)=>
      if data['errors']
        return JSON.parse data['errors']
      else
        @set_user()
    )
    
  register: (data)->
    @set_path('/users.json')
    register_data = data
    @save(data).$promise.then((data) =>
      if data['error']
        return data['error']
      else
        @login(register_data)
    )

@app.service('Auth', Auth)