class @Resource
  @$inject: ['$resource']
  
  constructor: (@resource)->
    @filters = {}
    @path=''
    
  set_filter: (name, value="") ->
    @filters["filters["+name+"]"] = value
    @
    
  clear_filters: ->
    @filters = {}
    
  set_path: (path) ->
    @path = path
    @
    
  get_path: ->
    @path

  query: () ->
    path = @get_path()
    @perform_request(@resource(path).query(@filters))
    
  get: (id)->
    path = @get_path()
    @resource(path, {id: '@id'})
    
  save: (data)->
    path = @get_path()
    @resource(path).save(data)
  
  perform_request: (resource)->
      @clear_filters()
      resource  
    
@app.service('Resource', Resource)