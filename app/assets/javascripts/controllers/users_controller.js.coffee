class UsersController
  @$inject: ['$scope', 'User', 'Auth', '$http']
  
  constructor: (@scope, User, @Auth, @http) ->
    @scope.users = User.set_filter("without_friends").query()
  add_to_friends: (user)->
    @http.post('/friendships.json', {friend_id: user.id})
    .success((response)=>
      user.added = true
    )
@app.controller 'UsersController', UsersController