class ApplicationController
  @$inject: ['$scope','Auth']
  
  constructor: ($scope,Auth) ->
    $scope.current_user = Auth.current_user()
@app.controller 'ApplicationController', ApplicationController