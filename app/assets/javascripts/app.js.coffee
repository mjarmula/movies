@app = angular.module 'app', [
  'ngResource',
  'ngRoute',
  'ngAnimate',
  'ui.bootstrap'
]