class Alerts
  @$inject: ['$timeout']
  constructor: (@timeout)->
    @alerts = []
    
  add_alert: (alert)->
    @alerts.push alert
    @timeout =>
      @remove_alert alert
    , 3000
    
  remove_alert: (alert) ->
    alert_index = @alerts.indexOf alert
    @alerts.splice alert_index, 1
  
@app.service 'Alerts', Alerts