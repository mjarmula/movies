class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  attr_reader :current_user
  
  has_many :invited_me, class_name: "Friendship", foreign_key: "invited_id"
  has_many :i_invited, class_name: "Friendship", foreign_key: "inviter_id"
  has_many :friends_i_invited, through: :i_invited, source: :invited
  has_many :friends_invited_me, through: :invited_me, source: :inviter
  
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  
  include Filtrable
  
  scope :get_user, ->(id) { where id: id }
         
  def my_friends
    self.friends_i_invited + self.friends_invited_me
  end

end
