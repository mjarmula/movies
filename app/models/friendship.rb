class Friendship < ActiveRecord::Base
  belongs_to :inviter, class_name: "User"
  belongs_to :invited, class_name: "User"
  
  validates :invited_id, uniqueness: {scope: :inviter_id, message: "This friendship already exists"}
  validates :inviter_id, presence: true
  validates :invited_id, presence: true
end
