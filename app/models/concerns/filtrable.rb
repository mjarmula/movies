module Filtrable
  extend ActiveSupport::Concern
  module ClassMethods
    def filter(filters)
      if !filters.blank?
        results = self.where(nil)
        filters.each do |name, args|
          next unless self.respond_to?(name,args) 
          results = args ? results.public_send(name, args) : results.public_send(name) 
        end
        results
      end
    end
  end
end