module Users
    class ShowPresenter
      def initialize(current_user)
        @current_user = current_user
      end
      def confirmed_users 
        @current_user.my_friends
      end
    end
end