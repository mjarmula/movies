class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :authenticate_user!, only: [:show, :index]
  before_action :configure_permitted_parameters, if: :devise_controller?
  layout :set_layout
  after_filter :set_csrf_cookie_for_ng
  
  protected
  
  def set_csrf_cookie_for_ng
    cookies['XSRF-TOKEN'] = form_authenticity_token if protect_against_forgery?
  end
  
  def verified_request?
    super || valid_authenticity_token?(session, request.headers['X-XSRF-TOKEN'])
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:username])
  end
  
  def set_layout
    return false if params[:disable_layout].present?
    nil
  end
  
  def authenticate_user!(opts={}) 
    if user_signed_in?
      super
    else
      self.status = :unauthorized
      render json: Hash.new
    end
  end
  
end
