class FriendshipsController < ApplicationController
  def create
    Friendship.create({inviter_id: current_user.id, invited_id: params[:friend_id]})
    respond_to do |format|
      format.json { render json: {
        alert: {
          type: "success",
          msg: "Your friend has been added"
        }
        } }
    end
  end
  
  def destroy
    invited_id = params[:id]
    current_user.friends_i_invited.delete(invited_id)
    render json: {
      alert: {
        type: "success",
        msg: "Friendship has been removed"
      }
      }
  end
end