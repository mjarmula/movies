class UsersController < ApplicationController
  before_filter :authenticate_user!, :except => [:dashboard, :sign_in]
  def dashboard
  end
  
  def index
    respond_to do |format|
      format.html {}
      format.json do
        users = ::Filters::UsersFilter.new(self).run_filters
        render json: users 
      end 
    end 
  end
  
  def show
    respond_to do |format|
      format.html {}
      format.json do  
        users = ::Filters::UsersFilter.new(self).run_filters
        render json: @users 
      end
    end 
  end
  
  def sign_in
    #empty method only because of devise...
  end
  
end
