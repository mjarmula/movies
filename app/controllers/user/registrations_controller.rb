class User::RegistrationsController < Devise::RegistrationsController
  skip_before_filter :authenticate_user!, :only => :new
  before_filter :configure_permitted_parameter
  
  def create
    respond_to do |format|
      format.json do
        @user = User.new(user_params)
        if @user.save
          render json: { success: { data: @user } }
        else
          render json: { error: @user.errors.full_messages }
        end
      end
    end
  end
  
  protected
  
  def user_params
    params.require(:user).permit([:username, :password, :password_confirmation, :email])
  end
  
  def configure_permitted_parameter
    devise_parameter_sanitizer.permit(:sign_up, keys: [:username])
    devise_parameter_sanitizer.permit(:account_update, keys: [:username])
  end
end