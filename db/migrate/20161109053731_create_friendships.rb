class CreateFriendships < ActiveRecord::Migration
  def change
    create_table :friendships do |t|
      
      t.references :inviter
      t.references :invited     
      t.boolean :confirmed
      t.timestamps
    end
  end
end
