require 'test_helper'

class FriendshipTest < ActiveSupport::TestCase
  test 'check friendship uniqueness' do
    inviter = friendships(:one)
    duplicated_friendship = Friendship.new(inviter_id: 1, invited_id: 2)
    assert duplicated_friendship.invalid?
  end
  
  test 'check friendship attributes emptiness' do
    friendship = Friendship.new()
    assert friendship.invalid?
    friendship.inviter_id = 2
    assert friendship.invalid?
    friendship.inviter_id = nil
    friendship.invited_id = 2
    assert friendship.invalid?
    friendship.inviter_id = 100
    assert friendship.valid?
  end
end
